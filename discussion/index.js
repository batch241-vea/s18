// alert("qwe");
/*
	name is called parameter
	- parameter acts as a named variable/container that exists only inside of a function
	- it is used to store info that is provided to a function when it is called

	Syntax:
		function functionName(parameter){
			code block
		}
		functionName(argument);
*/
function printName(name){
	console.log("My name is " + name);
}

// Juan and Jenno, the info/data provided directly into the function, it is called an argument
printName("Juan");
printName("Jenno");

//Variables can also be passed as an argument
let userName = "Hakdog";

printName(userName);

// Using multiple Parameters

function creatingFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}
creatingFullName("Erven", "Joshua", "Cabral");
// will be assigned respectively

// the last assignment will be undefined
creatingFullName("Eric", "Andales");

// will ignore the excess argument
creatingFullName("Roland", "John", "Doroteo", "Jane");


function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is " + remainder);
	let isDivisibleBy8 = (remainder === 0);
	console.log("Does " + num + " is divible by 8?")
	console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

/*
	Mini Activity
	1. create a function which is able to receive data as an argument.
		- This function should be able to receive the name of your favorite superhero.
		- Display the name of your fav super hero in the console.
*/
	
	function displayFavSuperhero(fav){
		console.log("My favorite superhero is " + fav);
	}	
	displayFavSuperhero("Cardo Dalisay");

	// Return Statement
	/*
		The return statement allows us to output a value from a function to be passed to the line/block of code that invoke/called the function
	*/

	function returnFullName(firstName, middleName, lastName){
		return firstName + " " + middleName + " " + lastName
		console.log("Can we print this message?");
	}
	// Whatever value that is returned from the returnFullName function can be stored in a variable
	let completeName = returnFullName("John", "Doe", "Smith");
	console.log(completeName);

	console.log(returnFullName("One", "Two", "Three"));

	function returnAddress(city, country){
		let fullAddress = city + ", " + country;
		return fullAddress; 
	}
	let myAddress = returnAddress("Laoag City", "Philippines");
	console.log(myAddress);

	// Consider the ff code.
	function printPlayerInformation(userName, level, job){
		console.log("Username: " + userName);
		console.log("Level: " + level);
		console.log("Job: " + job);

		return userName + " " + level + " " + job;
	}

	let user1 = printPlayerInformation("cardo123", "999", "Immortal");
	console.log(user1);

	// returns undefined because printPlayerInformation returns nothing. It only logs the message in the console.
	// you cannot save any value from printPlayerInformation() because it does not RETURN anything.

	/*
		Mini Activity:
			1. Debug our code. So that the function will be able to return a value and save it in a variable.
	*/