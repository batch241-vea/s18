// console.log("Hello World");

function addTwoNumbers(num1, num2){
	let sum = num1 + num2;
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(sum);
}
function subtractTwoNumbers(num1, num2){
	let difference = num1 - num2;
	console.log("Displayed difference of " + num1 + " and " + num2);
	console.log(difference);
 }

addTwoNumbers(5, 15);
subtractTwoNumbers(20, 5);



function multiplyTwoNumbers(num1, num2){
	let productSentence = "The product of " + num1 + " and " + num2 + ":";
	let productSolution = num1 * num2;

	console.log(productSentence);
	return productSolution;	
 }
function divideTwoNumbers(num1, num2){
	let quotientSentence = "The quotient of " + num1 + " and " + num2 + ":";
	let quotientSolution = num1 / num2;

	console.log(quotientSentence);
	return quotientSolution;	
 }

let product = multiplyTwoNumbers(50, 10);
console.log(product);

let quotient = divideTwoNumbers(50, 10);
console.log(quotient);



function getTotalAreaOfCircle(radius, pie){
	let areaSentence = "The result of getting the area of a circle with " + radius + " radius:";
	let areaSolution = pie * (radius ** 2);

	console.log(areaSentence);
	return areaSolution;
}
let circleArea = getTotalAreaOfCircle(15, 3.14);
console.log(circleArea);



function getTotalAverage(num1, num2, num3, num4){
	let averageSentence = "The average of " + num1 + ", " + num2 + ", " + num3 + " and " + num4 + ":";
	let averageSolution = (num1 + num2 + num3 + num4)/4;

	console.log(averageSentence);
	return averageSolution;
}
let averageVar = getTotalAverage(20, 40, 60, 80);
console.log(averageVar);



function checkPassingPercentage(score, totalScore){
	let passingSentence = "Is " + score + "/" + totalScore + " a passing score?"
	let passingSolution = (score/totalScore)*100;
	let checkPassingSolution = (passingSolution >= 75);

	console.log(passingSentence);
	return checkPassingSolution;
}

let isPassed = checkPassingPercentage(38, 50);
console.log(isPassed);